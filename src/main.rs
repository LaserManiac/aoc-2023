use std::alloc::{GlobalAlloc, Layout, System};
use std::fmt::Display;
use std::sync::atomic::{AtomicU64, Ordering};

pub mod day1;
pub mod day10;
pub mod day11;
pub mod day12;
pub mod day13;
pub mod day14;
pub mod day15;
pub mod day16;
pub mod day17;
pub mod day18;
pub mod day19;
pub mod day2;
pub mod day20;
pub mod day3;
pub mod day4;
pub mod day5;
pub mod day6;
pub mod day7;
pub mod day8;
pub mod day9;

#[global_allocator]
static ALLOCATOR: StatsAllocator = StatsAllocator;

const BENCHMARK_ITERATIONS: u64 = 1;

static DAYS: &[(u8, [&(dyn DayExec + Sync); 2])] = &[
    (1, [&day1::a::run, &day1::b::run]),
    (2, [&day2::a::run, &day2::b::run]),
    (3, [&day3::a::run, &day3::b::run]),
    (4, [&day4::a::run, &day4::b::run]),
    (5, [&day5::a::run, &day5::b::run]),
    (6, [&day6::a::run, &day6::b::run]),
    (7, [&day7::a::run, &day7::b::run]),
    (8, [&day8::a::run, &day8::b::run]),
    (9, [&day9::a::run, &day9::b::run]),
    (10, [&day10::a::run, &day10::b::run]),
    (11, [&day11::a::run, &day11::b::run]),
    (12, [&day12::a::run, &day12::b::run]),
    (13, [&day13::a::run, &day13::b::run]),
    (14, [&day14::a::run, &day14::b::run]),
    (15, [&day15::a::run, &day15::b::run]),
    (16, [&day16::a::run, &day16::b::run]),
    (17, [&day17::a::run, &day17::b::run]),
    (18, [&day18::a::run, &day18::b::run]),
    (19, [&day19::a::run, &day19::b::run]),
    (20, [&day20::a::run, &day20::b::run]),
];

pub fn main() {
    run_days(&[20]);
}

fn run_days(filter: &[u8]) {
    for (day, [a, b]) in DAYS {
        if filter.is_empty() || filter.contains(day) {
            println!("\nDay {day}:");
            benchmark(|| Into::<Option<u64>>::into(a.run()), BENCHMARK_ITERATIONS);
            benchmark(|| Into::<Option<u64>>::into(b.run()), BENCHMARK_ITERATIONS);
        }
    }
}

fn benchmark<F: Fn() -> Option<R>, R: Display>(f: F, iter: u64) {
    let mut result = Option::<R>::None;
    let mut total_time = 0;
    let mut total_alloc = 0;

    for _ in 0..iter {
        ALLOCATOR.clear_alloc_count();
        let start = std::time::Instant::now();
        result = f();
        let elapsed = start.elapsed();
        total_time += elapsed.as_micros();
        total_alloc += ALLOCATOR.get_alloc_count();
    }

    let result = result
        .as_ref()
        .map(ToString::to_string)
        .unwrap_or_else(|| String::from("none"));
    let us = total_time / iter as u128;
    let allocations = total_alloc / iter;

    if us < 1000 {
        println!("{iter}i: {us:5}us | a: {allocations:<2} | Result: {result}");
    } else {
        let ms = us as f32 / 1000.0;
        println!("{iter}i: {ms:5.2}ms | a: {allocations:<2} | Result: {result}");
    }
}

pub trait DayExec {
    fn run(&self) -> Option<u64>;
}

impl<T, R> DayExec for T
where
    T: Fn() -> R,
    R: Into<Option<u64>>,
{
    fn run(&self) -> Option<u64> {
        (self)().into()
    }
}

struct StatsAllocator;

static ALLOCATION_COUNT: AtomicU64 = AtomicU64::new(0);

impl StatsAllocator {
    pub fn clear_alloc_count(&self) {
        ALLOCATION_COUNT.store(0, Ordering::Relaxed);
    }

    pub fn get_alloc_count(&self) -> u64 {
        ALLOCATION_COUNT.load(Ordering::Relaxed)
    }
}

unsafe impl GlobalAlloc for StatsAllocator {
    unsafe fn alloc(&self, layout: Layout) -> *mut u8 {
        ALLOCATION_COUNT.fetch_add(1, Ordering::Relaxed);
        return System.alloc(layout);
    }

    unsafe fn dealloc(&self, ptr: *mut u8, layout: Layout) {
        System.dealloc(ptr, layout);
    }
}
