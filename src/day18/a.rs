use std::str::FromStr;

use crate::day18::INPUT;

pub fn run() -> Option<u64> {
    let plan = INPUT
        .lines()
        .map(Instr::try_from)
        .flat_map(Result::ok)
        .collect::<Vec<_>>();

    let mut sum = 0i64;
    let mut x = 0i64;
    for i in 0..plan.len() {
        let instr = plan[i];

        let p = plan[(i + plan.len() - 1) % plan.len()];
        let pw = (p.dir.winding(instr.dir) != Some(Winding::Ccw)) as i64;

        let n = plan[(i + 1) % plan.len()];
        let nw = (instr.dir.winding(n.dir) != Some(Winding::Ccw)) as i64;

        let len = (instr.len - 1) as i64 + pw + nw;
        match instr.dir {
            Direction::Up => sum -= len * x,
            Direction::Left => x -= len,
            Direction::Down => sum += len * x,
            Direction::Right => x += len,
        }
    }

    return Some(sum.abs() as u64);
}

#[derive(Clone, Copy, Debug)]
struct Instr<'a> {
    dir: Direction,
    len: u64,
    _hex: &'a str,
}

impl<'a> TryFrom<&'a str> for Instr<'a> {
    type Error = ();

    fn try_from(source: &'a str) -> Result<Self, Self::Error> {
        let mut parts = source.split(' ');
        let dir = Direction::from_str(parts.next().ok_or(())?)?;
        let len = parts.next().ok_or(())?.parse().map_err(|_| ())?;
        let _hex = parts.next().ok_or(())?;
        return Ok(Self { dir, len, _hex });
    }
}

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub enum Direction {
    Up,
    Left,
    Down,
    Right,
}

impl FromStr for Direction {
    type Err = ();

    fn from_str(source: &str) -> Result<Self, Self::Err> {
        match source {
            "U" => Ok(Direction::Up),
            "L" => Ok(Direction::Left),
            "D" => Ok(Direction::Down),
            "R" => Ok(Direction::Right),
            _ => Err(()),
        }
    }
}

impl Direction {
    pub fn winding(self, other: Direction) -> Option<Winding> {
        if self.left() == other {
            Some(Winding::Ccw)
        } else if self.right() == other {
            Some(Winding::Cw)
        } else {
            None
        }
    }

    pub fn left(self) -> Self {
        match self {
            Self::Up => Self::Left,
            Self::Left => Self::Down,
            Self::Down => Self::Right,
            Self::Right => Self::Up,
        }
    }

    pub fn right(self) -> Self {
        match self {
            Self::Up => Self::Right,
            Self::Left => Self::Up,
            Self::Down => Self::Left,
            Self::Right => Self::Down,
        }
    }
}

#[derive(Clone, Copy, PartialEq, Eq)]
pub enum Winding {
    Cw,
    Ccw,
}
