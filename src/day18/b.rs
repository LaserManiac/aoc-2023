use std::str::FromStr;

use crate::day18::INPUT;

pub fn run() -> Option<u64> {
    let plan = INPUT
        .lines()
        .map(Instr::try_from)
        .flat_map(Result::ok)
        .collect::<Vec<_>>();

    let mut sum = 0i64;
    let mut x = 0i64;
    for i in 0..plan.len() {
        let instr = plan[i];

        let p = plan[(i + plan.len() - 1) % plan.len()];
        let pw = (p.dir.winding(instr.dir) != Some(Winding::Ccw)) as i64;

        let n = plan[(i + 1) % plan.len()];
        let nw = (instr.dir.winding(n.dir) != Some(Winding::Ccw)) as i64;

        let len = (instr.len - 1) as i64 + pw + nw;
        match instr.dir {
            Direction::Up => sum -= len * x,
            Direction::Left => x -= len,
            Direction::Down => sum += len * x,
            Direction::Right => x += len,
        }
    }

    return Some(sum.abs() as u64);
}

#[derive(Clone, Copy, Debug)]
struct Instr {
    dir: Direction,
    len: u64,
}

impl TryFrom<&str> for Instr {
    type Error = ();

    fn try_from(source: &str) -> Result<Self, Self::Error> {
        let hex = source.split(' ').skip(2).next().ok_or(())?;
        let hex = &hex[2..hex.len() - 1];
        let len = hex
            .chars()
            .take(5)
            .filter_map(|c| c.to_digit(16))
            .fold(0, |a, d| a * 16 + d) as u64;
        let dir_idx = hex.chars().last().ok_or(())?.to_digit(4).ok_or(())? as usize;
        let dir = [
            Direction::Right,
            Direction::Down,
            Direction::Left,
            Direction::Up,
        ][dir_idx];
        return Ok(Self { dir, len });
    }
}

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub enum Direction {
    Up,
    Left,
    Down,
    Right,
}

impl FromStr for Direction {
    type Err = ();

    fn from_str(source: &str) -> Result<Self, Self::Err> {
        match source {
            "U" => Ok(Direction::Up),
            "L" => Ok(Direction::Left),
            "D" => Ok(Direction::Down),
            "R" => Ok(Direction::Right),
            _ => Err(()),
        }
    }
}

impl Direction {
    pub fn winding(self, other: Direction) -> Option<Winding> {
        if self.left() == other {
            Some(Winding::Ccw)
        } else if self.right() == other {
            Some(Winding::Cw)
        } else {
            None
        }
    }

    pub fn left(self) -> Self {
        match self {
            Self::Up => Self::Left,
            Self::Left => Self::Down,
            Self::Down => Self::Right,
            Self::Right => Self::Up,
        }
    }

    pub fn right(self) -> Self {
        match self {
            Self::Up => Self::Right,
            Self::Left => Self::Up,
            Self::Down => Self::Left,
            Self::Right => Self::Down,
        }
    }
}

#[derive(Clone, Copy, PartialEq, Eq)]
pub enum Winding {
    Cw,
    Ccw,
}
