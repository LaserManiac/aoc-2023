use std::str::FromStr;

use crate::day17::INPUT;

pub fn run() -> Option<u64> {
    let map = Map::from_str(INPUT).ok()?;
    return map.walk(0, 0, map.width() - 1, map.height() - 1);
}

const MAX_STEPS: u8 = 10;
const STEPS_BEFORE_TURN: u8 = 4;

struct Map {
    width: usize,
    height: usize,
    tiles: Vec<u8>,
}

impl FromStr for Map {
    type Err = ();

    fn from_str(source: &str) -> Result<Self, Self::Err> {
        let mut tiles = Vec::new();
        for line in source.lines() {
            for b in line.bytes() {
                let value = b - b'0';
                tiles.push(value);
            }
        }
        let height = source.lines().count();
        let width = tiles.len() / height;
        return Ok(Self {
            width,
            height,
            tiles,
        });
    }
}

impl Map {
    pub fn width(&self) -> usize {
        self.width
    }

    pub fn height(&self) -> usize {
        self.height
    }

    pub fn heat(&self, x: usize, y: usize) -> u8 {
        self.tiles[y * self.width + x]
    }

    pub fn walk(&self, x: usize, y: usize, tx: usize, ty: usize) -> Option<u64> {
        let mut wavefront = Wavefront::default();

        for dir in Direction::ALL {
            if let Some(_) = self.mov(x, y, dir, 1) {
                wavefront.add(Ripple::new(x, y, dir, 0, 0));
            }
        }

        let mut cache = HeatCache::new(self.width, self.height);
        while let Some(ripple) = wavefront.next() {
            if ripple.x == tx && ripple.y == ty && ripple.can_turn() {
                return Some(ripple.heat);
            }

            for mov in ripple.moves() {
                let Some((dir, step, len)) = mov else {
                    continue;
                };
                let Some((x, y, heat)) = self.mov(ripple.x, ripple.y, dir, len) else {
                    continue;
                };
                let heat = ripple.heat + heat;
                let ripple = Ripple::new(x, y, dir, heat, step);
                if cache.update(&ripple) {
                    wavefront.add(ripple);
                }
            }
        }

        return None;
    }

    fn mov(&self, x: usize, y: usize, dir: Direction, len: usize) -> Option<(usize, usize, u64)> {
        match dir {
            Direction::Up if y >= len => Some((
                x,
                y - len,
                (0..len).map(|d| self.heat(x, y - d - 1) as u64).sum(),
            )),
            Direction::Left if x >= len => Some((
                x - len,
                y,
                (0..len).map(|d| self.heat(x - d - 1, y) as u64).sum(),
            )),
            Direction::Down if y <= self.height - len - 1 => Some((
                x,
                y + len,
                (0..len).map(|d| self.heat(x, y + d + 1) as u64).sum(),
            )),
            Direction::Right if x <= self.width - len - 1 => Some((
                x + len,
                y,
                (0..len).map(|d| self.heat(x + d + 1, y) as u64).sum(),
            )),
            _ => None,
        }
    }
}

pub struct HeatCache {
    width: usize,
    data: Vec<[u64; MAX_STEPS as usize]>,
}

impl HeatCache {
    pub fn new(width: usize, height: usize) -> Self {
        let len = width * height * MAX_STEPS as usize * Direction::ALL.len();
        let data = vec![[u64::MAX; MAX_STEPS as usize]; len];
        return Self { width, data };
    }

    pub fn update(&mut self, ripple: &Ripple) -> bool {
        let idx = self.idx(ripple.x, ripple.y, ripple.dir);
        let min_heats = &mut self.data[idx];

        for step in 0..ripple.step + 1 {
            let min_heat = &mut min_heats[step as usize];

            if *min_heat <= ripple.heat {
                return false;
            }

            if step == ripple.step {
                *min_heat = ripple.heat;
            }
        }

        return true;
    }

    fn idx(&self, x: usize, y: usize, dir: Direction) -> usize {
        let pos_idx = y * self.width + x;
        let dir_idx = pos_idx * Direction::ALL.len() + dir.idx();
        return dir_idx;
    }
}

#[derive(Default)]
pub struct Wavefront {
    ripples: Vec<Ripple>,
}

impl Wavefront {
    pub fn next(&mut self) -> Option<Ripple> {
        self.ripples.pop()
    }

    pub fn add(&mut self, ripple: Ripple) {
        let idx = self
            .ripples
            .binary_search_by(|r| r.heat.cmp(&ripple.heat).reverse());
        let idx = match idx {
            Ok(idx) => idx,
            Err(idx) => idx,
        };
        self.ripples.insert(idx, ripple);
    }
}

#[derive(Clone, Copy, Debug)]
pub struct Ripple {
    x: usize,
    y: usize,
    dir: Direction,
    heat: u64,
    step: u8,
}

impl Ripple {
    pub fn new(x: usize, y: usize, dir: Direction, heat: u64, step: u8) -> Self {
        Self {
            x,
            y,
            dir,
            heat,
            step,
        }
    }

    pub fn can_continue(&self) -> bool {
        self.step + 1 < MAX_STEPS
    }

    pub fn can_turn(&self) -> bool {
        self.step + 1 >= STEPS_BEFORE_TURN
    }

    pub fn moves(&self) -> [Option<(Direction, u8, usize)>; 3] {
        let target_fwd = (self.step + 1).max(STEPS_BEFORE_TURN - 1);
        return [
            self.can_continue().then_some((
                self.dir,
                target_fwd,
                target_fwd.saturating_sub(self.step) as usize,
            )),
            self.can_turn().then_some((
                self.dir.left(),
                STEPS_BEFORE_TURN - 1,
                STEPS_BEFORE_TURN as usize,
            )),
            self.can_turn().then_some((
                self.dir.right(),
                STEPS_BEFORE_TURN - 1,
                STEPS_BEFORE_TURN as usize,
            )),
        ];
    }
}

#[derive(Clone, Copy, PartialEq, Eq, Hash, Debug)]
pub enum Direction {
    Up,
    Left,
    Down,
    Right,
}

impl Direction {
    pub const ALL: [Direction; 4] = [
        Direction::Up,
        Direction::Left,
        Direction::Down,
        Direction::Right,
    ];

    pub fn idx(self) -> usize {
        match self {
            Self::Up => 0,
            Self::Left => 1,
            Self::Down => 2,
            Self::Right => 3,
        }
    }

    pub fn invert(self) -> Self {
        match self {
            Self::Up => Self::Down,
            Self::Left => Self::Right,
            Self::Down => Self::Up,
            Self::Right => Self::Left,
        }
    }

    pub fn left(self) -> Self {
        match self {
            Self::Up => Self::Left,
            Self::Left => Self::Down,
            Self::Down => Self::Right,
            Self::Right => Self::Up,
        }
    }

    pub fn right(self) -> Self {
        match self {
            Self::Up => Self::Right,
            Self::Left => Self::Up,
            Self::Down => Self::Left,
            Self::Right => Self::Down,
        }
    }
}
