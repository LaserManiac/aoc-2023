use crate::day1::INPUT;

pub fn run() -> u64 {
    return INPUT.lines().map(parse_line).sum::<u32>() as u64;
}

fn parse_line(line: &str) -> u32 {
    let mut first_idx = line.len();
    let mut first_value = 0;
    let mut last_idx = 0;
    let mut last_value = 0;
    for (d, v) in DIGITS {
        let len = d.len();
        let mut start = 0;
        while let Some(idx) = line[start..].find(d) {
            let new_start = start + idx + len;
            let idx = std::mem::replace(&mut start, new_start) + idx;
            if idx < first_idx {
                first_idx = idx;
                first_value = v;
            }
            if idx + len > last_idx {
                last_idx = idx + len;
                last_value = v;
            }
        }
    }
    return first_value * 10 + last_value;
}

const DIGITS: [(&str, u32); 18] = [
    ("1", 1),
    ("2", 2),
    ("3", 3),
    ("4", 4),
    ("5", 5),
    ("6", 6),
    ("7", 7),
    ("8", 8),
    ("9", 9),
    ("one", 1),
    ("two", 2),
    ("three", 3),
    ("four", 4),
    ("five", 5),
    ("six", 6),
    ("seven", 7),
    ("eight", 8),
    ("nine", 9),
];
