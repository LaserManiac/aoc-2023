use crate::day1::INPUT;

pub fn run() -> u64 {
    const UNICODE_0: u32 = '0' as u32;
    const UNICODE_9: u32 = '9' as u32;
    let is_digit = |c: &&u8| **c as u32 >= UNICODE_0 && **c as u32 <= UNICODE_9;
    let parse_line = |line: &[u8]| {
        let a = *line.into_iter().filter(is_digit).next()? as u32 - UNICODE_0;
        let b = *line.into_iter().rev().filter(is_digit).next()? as u32 - UNICODE_0;
        return Some(a * 10 + b);
    };
    return INPUT
        .lines()
        .map(|line| line.as_bytes())
        .filter_map(parse_line)
        .sum::<u32>() as u64;
}
