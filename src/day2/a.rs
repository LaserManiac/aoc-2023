use crate::day2::INPUT;

pub fn run() -> u64 {
    return INPUT
        .lines()
        .filter_map(parse_line)
        .filter_map(|(i, r, g, b)| {
            (r <= RED_COUNT && g <= GREEN_COUNT && b <= BLUE_COUNT).then_some(i)
        })
        .sum::<u32>() as u64;
}

fn parse_line(line: &str) -> Option<(u32, u32, u32, u32)> {
    let mut parts = line.split(": ");
    let id = parts
        .next()?
        .split(" ")
        .skip(1)
        .next()?
        .parse::<u32>()
        .ok()?;
    let mut max_red = 0;
    let mut max_green = 0;
    let mut max_blue = 0;
    for cube in parts.next()?.split(", ").flat_map(|part| part.split("; ")) {
        let mut split = cube.split(" ");
        let count = split.next()?.parse::<u32>().ok()?;
        let color = split.next()?;
        let counter = match color {
            "red" => &mut max_red,
            "green" => &mut max_green,
            "blue" => &mut max_blue,
            _ => return None,
        };
        *counter = u32::max(*counter, count);
    }
    return Some((id, max_red, max_green, max_blue));
}

const RED_COUNT: u32 = 12;
const GREEN_COUNT: u32 = 13;
const BLUE_COUNT: u32 = 14;
