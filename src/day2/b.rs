use crate::day2::INPUT;

pub fn run() -> u64 {
    return INPUT.lines().filter_map(parse_line).sum::<u32>() as u64;
}

fn parse_line(line: &str) -> Option<u32> {
    let mut parts = line.split(": ").skip(1);
    let mut min_red = 0;
    let mut min_green = 0;
    let mut min_blue = 0;
    for cube in parts.next()?.split(", ").flat_map(|part| part.split("; ")) {
        let mut split = cube.split(" ");
        let count = split.next()?.parse::<u32>().ok()?;
        let color = split.next()?;
        let counter = match color {
            "red" => &mut min_red,
            "green" => &mut min_green,
            "blue" => &mut min_blue,
            _ => return None,
        };
        *counter = u32::max(*counter, count);
    }
    return Some(min_red * min_green * min_blue);
}
