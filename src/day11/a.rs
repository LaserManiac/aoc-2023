use crate::day11::INPUT;

const EXPANSION_RATE: usize = 2;

pub fn run() -> Option<u64> {
    let mut width = 0;
    let mut height = 0;
    let mut galaxies = Vec::new();

    for (y, line) in INPUT.lines().enumerate() {
        height = height.max(y + 1);
        for (x, c) in line.bytes().enumerate() {
            width = width.max(x + 1);
            if c == b'#' {
                galaxies.push((x, y));
            }
        }
    }

    let mut lines = vec![Line::default(); width + height];
    for (x, y) in &galaxies {
        let horizontal = &mut lines[*x];
        horizontal.empty = false;

        let vertical = &mut lines[width + *y];
        vertical.empty = false;
    }

    for line in &mut lines {
        if line.empty {
            line.expansion = EXPANSION_RATE - 1;
        }
    }

    for x in 1..width {
        lines[x].expansion += lines[x - 1].expansion;
    }

    for y in 1..height {
        lines[width + y].expansion += lines[width + y - 1].expansion;
    }

    for (x, y) in &mut galaxies {
        *x += lines[*x].expansion;
        *y += lines[width + *y].expansion;
    }

    let mut result = 0;
    for i in 0..galaxies.len() {
        let (ax, ay) = galaxies[i];
        for j in i + 1..galaxies.len() {
            let (bx, by) = galaxies[j];
            let dx = ax.abs_diff(bx);
            let dy = ay.abs_diff(by);
            result += dx + dy;
        }
    }

    return Some(result as u64);
}

#[derive(Clone, Copy)]
struct Line {
    empty: bool,
    expansion: usize,
}

impl Default for Line {
    fn default() -> Self {
        Self {
            empty: true,
            expansion: 0,
        }
    }
}
