use std::cmp::Ordering;

use crate::day7::INPUT;

pub fn run() -> u64 {
    let mut hands = Vec::with_capacity(1000);
    hands.extend(INPUT.lines().filter_map(Hand::parse));
    hands.sort();
    return hands
        .iter()
        .enumerate()
        .map(|(idx, hand)| hand.winning(idx))
        .sum::<u64>();
}

fn hand_type(hand: &[u8]) -> HandType {
    let mut counts = [0u8; CARDS.len()];
    for c in hand {
        let idx = card_idx(*c);
        counts[idx] += 1;
    }

    let mut sets = [0u8; 5];
    for value in counts {
        if value > 0 {
            let idx = (value - 1) as usize;
            sets[idx] += 1;
        }
    }

    return match sets {
        [0, 0, 0, 0, 1] => HandType::FiveOfAKind,
        [1, 0, 0, 1, 0] => HandType::FourOfAKind,
        [0, 1, 1, 0, 0] => HandType::FullHouse,
        [2, 0, 1, 0, 0] => HandType::ThreeOfAKind,
        [1, 2, 0, 0, 0] => HandType::TwoPair,
        [3, 1, 0, 0, 0] => HandType::OnePair,
        _ => HandType::HighCard,
    };
}

const CARDS: &[u8] = b"23456789TJQKA";

#[inline(always)]
fn card_idx(card: u8) -> usize {
    CARDS.iter().position(|x| *x == card).unwrap_or_default()
}

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord)]
enum HandType {
    HighCard,
    OnePair,
    TwoPair,
    ThreeOfAKind,
    FullHouse,
    FourOfAKind,
    FiveOfAKind,
}

#[derive(Debug, PartialEq, Eq, Ord)]
struct Hand<'a> {
    cards: &'a [u8],
    bid: u64,
    typ: HandType,
}

impl<'a> Hand<'a> {
    #[inline(always)]
    pub fn parse(line: &'a str) -> Option<Self> {
        let mut parts = line.split(' ');
        let cards = parts.next()?.as_bytes();
        let bid = parts.next()?.parse::<u64>().ok()?;
        let typ = hand_type(cards);
        return Some(Hand { cards, bid, typ });
    }

    #[inline(always)]
    pub fn winning(&self, idx: usize) -> u64 {
        (idx + 1) as u64 * self.bid
    }
}

impl<'a> PartialOrd<Self> for Hand<'a> {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        self.typ
            .cmp(&other.typ)
            .then_with(|| compare_cards(self.cards, other.cards))
            .into()
    }
}

fn compare_cards(a: &[u8], b: &[u8]) -> Ordering {
    for i in 0..a.len() {
        let idx_a = card_idx(a[i]);
        let idx_b = card_idx(b[i]);
        if idx_a > idx_b {
            return Ordering::Greater;
        } else if idx_a < idx_b {
            return Ordering::Less;
        }
    }
    return Ordering::Equal;
}
