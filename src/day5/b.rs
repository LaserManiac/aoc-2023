use crate::day5::INPUT;

pub fn run() -> u64 {
    return exec().unwrap();
}

fn exec() -> Option<u64> {
    let mut blocks = INPUT.split("\r\n\r\n").filter(|block| !block.is_empty());
    let mut seeds = blocks
        .next()?
        .split(": ")
        .skip(1)
        .next()?
        .split(' ')
        .map(str::parse::<u64>)
        .filter_map(Result::ok);

    let mut seed_src = std::iter::from_fn(|| Some((seeds.next()?, seeds.next()?)))
        .map(|(start, len)| (start, start + len - 1))
        .collect::<Vec<_>>();
    let mut seed_src_cache = Vec::with_capacity(seed_src.len());
    let mut seed_dst = Vec::with_capacity(seed_src.len());

    for block in blocks {
        parse_block(block, &mut seed_src, &mut seed_src_cache, &mut seed_dst);
        seed_src.clear();
        seed_src.extend_from_slice(&seed_dst);
        seed_dst.clear();
        seed_src_cache.clear();
    }

    return seed_src.into_iter().map(|(start, _)| start).min();
}

fn parse_block(
    block: &str,
    seed_src: &mut Vec<(u64, u64)>,
    seed_src_cache: &mut Vec<(u64, u64)>,
    seed_dst: &mut Vec<(u64, u64)>,
) -> Option<()> {
    for line in block.lines().skip(1) {
        let mut numbers = line.split(' ');
        let dst_start = numbers.next()?.parse::<u64>().ok()?;
        let src_start = numbers.next()?.parse::<u64>().ok()?;
        let len = numbers.next()?.parse::<u64>().ok()?;
        let dst_end = dst_start + len - 1;
        let src_end = src_start + len - 1;
        for src in seed_src.iter() {
            if src_start > src.1 || src_end < src.0 {
                // |...| <...> or <...> |...|
                seed_src_cache.push(*src);
            } else if src_start <= src.0 && src_end >= src.1 {
                // <.|...|.>
                seed_dst.push((
                    dst_start + (src.0 - src_start),
                    dst_start + (src.1 - src_start),
                ));
            } else if src_start > src.0 && src_end >= src.1 {
                // |.<.|.>
                seed_src_cache.push((src.0, src_start - 1));
                seed_dst.push((dst_start, dst_start + (src.1 - src_start)));
            } else if src_start <= src.0 && src_end < src.1 {
                // <.|.>.|
                seed_src_cache.push((src_end + 1, src.1));
                seed_dst.push((dst_start + (src.0 - src_start), dst_end));
            } else if src_start > src.0 && src_end < src.1 {
                // |.<...>.|
                seed_src_cache.extend_from_slice(&[(src.0, src_start - 1), (src_end + 1, src.1)]);
                seed_dst.push((dst_start, dst_end));
            } else {
                unreachable!();
            }
        }
        std::mem::swap(seed_src, seed_src_cache);
        seed_src_cache.clear();
    }
    seed_dst.extend_from_slice(&seed_src);
    return Some(());
}
