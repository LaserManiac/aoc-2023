use crate::day5::INPUT;

pub fn run() -> u64 {
    return exec().unwrap();
}

fn exec() -> Option<u64> {
    let mut blocks = INPUT.split("\r\n\r\n").filter(|block| !block.is_empty());
    let mut seed_src = blocks
        .next()?
        .split(": ")
        .skip(1)
        .next()?
        .split(' ')
        .map(str::parse::<u64>)
        .filter_map(Result::ok)
        .collect::<Vec<u64>>();
    let mut seed_dst = seed_src.clone();

    for block in blocks {
        parse_block(block, &mut seed_src, &mut seed_dst);
        seed_src.clear();
        seed_src.extend(seed_dst.iter().copied());
    }

    return seed_src.into_iter().min();
}

fn parse_block(block: &str, seed_src: &mut [u64], seed_dst: &mut [u64]) -> Option<()> {
    for line in block.lines().skip(1) {
        let mut numbers = line.split(' ');
        let dst_start = numbers.next()?.parse::<u64>().ok()?;
        let src_start = numbers.next()?.parse::<u64>().ok()?;
        let len = numbers.next()?.parse::<u64>().ok()?;
        let src_end = src_start + len - 1;
        for (src, dst) in seed_src.iter_mut().zip(seed_dst.iter_mut()) {
            if *src >= src_start && *src <= src_end {
                *dst = dst_start + (*src - src_start);
            }
        }
    }
    return Some(());
}
