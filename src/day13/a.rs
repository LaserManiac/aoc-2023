use crate::day13::INPUT;

pub fn run() -> Option<u64> {
    let mut map = Vec::new();
    let mut result = 0;
    for chunk in INPUT.split("\r\n\r\n") {
        let mut width = 0;
        for line in chunk.lines() {
            map.extend(line.bytes().map(|x| x == b'#'));
            width = line.len();
        }
        let height = map.len() / width;

        'outer: for x in 0..width - 1 {
            let i = (width - x - 1).min(x + 1);
            for dx in 0..i {
                for y in 0..height {
                    if map[y * width + x - dx] != map[y * width + x + dx + 1] {
                        continue 'outer;
                    }
                }
            }
            result += x + 1;
            break;
        }

        'outer: for y in 0..height - 1 {
            let i = (height - y - 1).min(y + 1);
            for dy in 0..i {
                for x in 0..width {
                    if map[(y - dy) * width + x] != map[(y + dy + 1) * width + x] {
                        continue 'outer;
                    }
                }
            }
            result += (y + 1) * 100;
            break;
        }

        map.clear();
    }
    return Some(result as u64);
}
