use crate::day13::INPUT;

pub fn run() -> Option<u64> {
    let mut map = Vec::new();
    let mut result = 0;
    for chunk in INPUT.split("\r\n\r\n") {
        let mut width = 0;
        for line in chunk.lines() {
            map.extend(line.bytes().map(|x| x == b'#'));
            width = line.len();
        }
        let height = map.len() / width;

        let skip = reflection(&map, width, height, None);
        for idx in 0..map.len() {
            let original = map[idx];
            map[idx] = !original;

            if let Some(reflection) = reflection(&map, width, height, skip) {
                let (score, dir) = reflection;
                result += match dir {
                    true => score,        // horizontal reflection
                    false => score * 100, // vertical reflection
                };
                break;
            }

            map[idx] = original;
        }

        map.clear();
    }
    return Some(result as u64);
}

fn reflection(
    map: &[bool],
    width: usize,
    height: usize,
    skip: Option<(usize, bool)>,
) -> Option<(usize, bool)> {
    let skip_hor = match skip {
        Some((skip, true)) => Some(skip),
        _ => None,
    };
    if let Some(score) = reflection_horizontal(map, width, height, skip_hor) {
        return Some((score, true));
    }

    let skip_vert = match skip {
        Some((skip, false)) => Some(skip),
        _ => None,
    };
    if let Some(score) = reflection_vertical(map, width, height, skip_vert) {
        return Some((score, false));
    }

    return None;
}

fn reflection_horizontal(
    map: &[bool],
    width: usize,
    height: usize,
    skip: Option<usize>,
) -> Option<usize> {
    'outer: for x in 0..width - 1 {
        if let Some(skip) = skip {
            if x + 1 == skip {
                continue;
            }
        }

        let i = (width - x - 1).min(x + 1);
        for dx in 0..i {
            for y in 0..height {
                if map[y * width + x - dx] != map[y * width + x + dx + 1] {
                    continue 'outer;
                }
            }
        }

        return Some(x + 1);
    }
    return None;
}

fn reflection_vertical(
    map: &[bool],
    width: usize,
    height: usize,
    skip: Option<usize>,
) -> Option<usize> {
    'outer: for y in 0..height - 1 {
        if let Some(skip) = skip {
            if y + 1 == skip {
                continue;
            }
        }

        let i = (height - y - 1).min(y + 1);
        for dy in 0..i {
            for x in 0..width {
                if map[(y - dy) * width + x] != map[(y + dy + 1) * width + x] {
                    continue 'outer;
                }
            }
        }

        return Some(y + 1);
    }
    return None;
}
