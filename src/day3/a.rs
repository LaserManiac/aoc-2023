use std::collections::HashMap;

use crate::day3::INPUT;

pub fn run() -> u64 {
    let mut numbers = Vec::<(u32, usize, usize, usize)>::new();
    let mut symbols = HashMap::<(usize, usize), char>::new();
    for (y, line) in INPUT.lines().enumerate() {
        let mut number = None;
        let mut number_start = 0;
        for (x, c) in line.chars().chain(std::iter::once('.')).enumerate() {
            if let Some(digit) = c.to_digit(10) {
                if let Some(number) = &mut number {
                    *number = *number * 10 + digit;
                } else {
                    number = Some(digit);
                    number_start = x;
                }
                continue;
            }

            if let Some(value) = number {
                numbers.push((value, number_start, x, y));
                number = None;
            }

            if c != '.' {
                symbols.insert((x, y), c);
            }
        }
    }

    return numbers
        .iter()
        .filter_map(|(number, from_x, to_x, y)| {
            let from_x = from_x.saturating_sub(1);
            let to_x = *to_x;
            let top_y = y.saturating_sub(1);
            let bottom_y = y + 1;
            let coords = [(from_x, *y), (to_x, *y)]
                .into_iter()
                .chain((from_x..=to_x).flat_map(|x| [(x, top_y), (x, bottom_y)]));
            for coord in coords {
                if symbols.contains_key(&coord) {
                    return Some(number);
                }
            }
            return None;
        })
        .sum::<u32>() as u64;
}
