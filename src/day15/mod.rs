pub mod a;
pub mod b;

pub const INPUT: &'static str = include_str!("./input.txt");

fn hash(bytes: &[u8]) -> u64 {
    let mut value = 0;
    for &b in bytes {
        value += b as u64;
        value *= 17;
        value %= 256;
    }
    return value;
}
