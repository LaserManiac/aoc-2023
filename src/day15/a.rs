use crate::day15::{hash, INPUT};

pub fn run() -> u64 {
    INPUT.split(',').map(str::as_bytes).map(hash).sum()
}
