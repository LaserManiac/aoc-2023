use std::collections::HashMap;

use crate::day15::{hash, INPUT};

pub fn run() -> u64 {
    let mut labels = Labels::default();
    let mut boxes = [Boxx::default(); 256];
    INPUT
        .split(',')
        .filter_map(|instr| Instruction::new(instr, &mut labels))
        .for_each(|instruction| {
            let boxx = &mut boxes[instruction.boxx];
            match instruction.focus {
                Some(focus) => boxx.set(instruction.label, focus),
                None => boxx.remove(instruction.label),
            }
        });
    return boxes
        .iter()
        .enumerate()
        .map(|(idx, boxx)| boxx.power((idx + 1) as u64))
        .sum();
}

type Label = u32;
type Focus = u8;

struct Instruction {
    label: Label,
    boxx: usize,
    focus: Option<Focus>,
}

impl Instruction {
    pub fn new<'a>(source: &'a str, labels: &mut Labels<'a>) -> Option<Self> {
        let mut instr = source.split('=').flat_map(|s| s.split('-'));
        let label = instr.next()?;
        let (label, boxx) = labels.get(label);
        let focus = instr.map(str::parse::<u8>).filter_map(Result::ok).next();
        return Some(Self { label, boxx, focus });
    }
}

#[derive(Default)]
struct Labels<'a> {
    counter: Label,
    cache: HashMap<&'a str, (Label, usize)>,
}

impl<'a> Labels<'a> {
    pub fn get(&mut self, label: &'a str) -> (Label, usize) {
        *self.cache.entry(label).or_insert_with(|| {
            self.counter += 1;
            let name = self.counter;
            let hash = hash(label.as_bytes()) as usize;
            return (name, hash);
        })
    }
}

#[derive(Clone, Copy, Default)]
struct Boxx {
    len: usize,
    slots: [Slot; 10],
}

impl Boxx {
    pub fn set(&mut self, label: Label, focus: Focus) {
        for slot in &mut self.slots {
            if slot.label == label {
                slot.focus = focus;
                return;
            }
        }
        self.len += 1;
        self.slots[self.len - 1] = Slot { label, focus };
    }

    pub fn remove(&mut self, label: Label) {
        for i in 0..self.len {
            if self.slots[i].label == label {
                for j in i..self.len - 1 {
                    self.slots[j] = self.slots[j + 1];
                }
                self.slots[self.len - 1] = Slot::default();
                self.len -= 1;
            }
        }
    }

    pub fn power(&self, box_id: u64) -> u64 {
        let mut power = 0;
        for i in 0..self.len {
            let slot_id = (i + 1) as u64;
            let focus = self.slots[i].focus as u64;
            power += box_id * slot_id * focus;
        }
        return power;
    }
}

#[derive(Clone, Copy, Default)]
struct Slot {
    label: Label,
    focus: Focus,
}
