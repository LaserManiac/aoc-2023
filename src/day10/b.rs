use std::collections::VecDeque;

use crate::day10::INPUT;

pub fn run() -> Option<u64> {
    let mut lines = INPUT.lines().peekable();
    let map_width = lines.peek()?.len();
    let mut map = Vec::with_capacity(20_000);
    let mut start = (0, 0);
    for (y, line) in lines.enumerate() {
        for (x, c) in line.bytes().enumerate() {
            let is_start = c == b'S';
            map.push((Tile::from(c), is_start, false, Option::<Direction>::None));
            if is_start {
                start = (x, y);
            }
        }
    }
    let map_height = map.len() / map_width;
    let idx = |x: usize, y: usize| y * map_width + x;

    let mut queue = VecDeque::with_capacity(100);
    let (x, y) = start;
    let to_visit = [
        (x.saturating_sub(1), y, Direction::Right),
        ((x + 1).min(map_width - 1), y, Direction::Left),
        (x, y.saturating_sub(1), Direction::Down),
        (x, (y + 1).min(map_height - 1), Direction::Up),
    ];
    let connects: [bool; 4] = std::array::from_fn(|i| {
        let (x, y, dir) = to_visit[i];
        return map[idx(x, y)].0.connects(dir);
    });
    let start_type = match connects {
        [true, true, false, false] => Tile::Horizontal,
        [false, false, true, true] => Tile::Vertical,
        [true, false, true, false] => Tile::BottomRight,
        [true, false, false, true] => Tile::TopRight,
        [false, true, true, false] => Tile::BottomLeft,
        [false, true, false, true] => Tile::TopLeft,
        _ => unreachable!(),
    };
    map[idx(start.0, start.1)].0 = start_type;

    for (x, y, dir) in to_visit {
        let tile = map[idx(x, y)];
        if tile.0.connects(dir) {
            queue.push_front((x, y, dir));
            let start_from = start_type.next(dir.invert()).unwrap();
            map[idx(start.0, start.1)].3 = Some(start_from);
            break;
        }
    }

    let mut trot = 0i64;
    while let Some((x, y, from)) = queue.pop_front() {
        let tile = &mut map[idx(x, y)];
        tile.1 = true;
        tile.3 = Some(from);
        trot += tile.0.rot(from);

        let Some(next_dir) = tile.0.next(from) else {
            unreachable!();
        };
        let next_pos = match next_dir {
            Direction::Left => (x.saturating_sub(1), y),
            Direction::Right => ((x + 1).min(map_width - 1), y),
            Direction::Up => (x, y.saturating_sub(1)),
            Direction::Down => (x, (y + 1).min(map_height - 1)),
        };

        if !map[idx(next_pos.0, next_pos.1)].1 {
            queue.push_back((next_pos.0, next_pos.1, next_dir.invert()));
        }
    }

    let mut queue = VecDeque::with_capacity(100_000);
    queue.push_back((start.0, start.1));
    let mut counter = 0;
    while let Some((x, y)) = queue.pop_back() {
        let idx = idx(x, y);
        let tile = &mut map[idx];

        if tile.2 {
            continue;
        } else {
            tile.2 = true;
            counter += 1;
        }

        if let Some(dir) = tile.3 {
            let out = if trot > 0 {
                dir.invert().left().invert()
            } else {
                dir.invert().left()
            };
            let next = tile.0.next(dir).unwrap();
            let next_out = if trot > 0 {
                next.left().invert()
            } else {
                next.left()
            };
            for ndir in [out, next, next_out] {
                queue.push_back(match ndir {
                    Direction::Left => (x.saturating_sub(1), y),
                    Direction::Right => ((x + 1).min(map_width - 1), y),
                    Direction::Up => (x, y.saturating_sub(1)),
                    Direction::Down => (x, (y + 1).min(map_height - 1)),
                });
            }
        } else {
            queue.push_back((x.saturating_sub(1), y));
            queue.push_back(((x + 1).min(map_width - 1), y));
            queue.push_back((x, y.saturating_sub(1)));
            queue.push_back((x, (y + 1).min(map_height - 1)));
        }
    }

    let map_area = map.len() as u64;
    let outside_loop = counter as u64;
    return Some(map_area - outside_loop);
}

#[derive(Clone, Copy, Debug)]
enum Direction {
    Up,
    Down,
    Left,
    Right,
}

impl Direction {
    pub fn invert(self) -> Direction {
        match self {
            Direction::Left => Direction::Right,
            Direction::Right => Direction::Left,
            Direction::Up => Direction::Down,
            Direction::Down => Direction::Up,
        }
    }

    pub fn left(self) -> Direction {
        match self {
            Direction::Left => Direction::Down,
            Direction::Right => Direction::Up,
            Direction::Up => Direction::Left,
            Direction::Down => Direction::Right,
        }
    }
}

#[derive(Clone, Copy, Debug)]
enum Tile {
    Vertical,
    Horizontal,
    TopLeft,
    TopRight,
    BottomLeft,
    BottomRight,
    Empty,
}

impl From<u8> for Tile {
    fn from(value: u8) -> Self {
        match value {
            b'|' => Self::Vertical,
            b'-' => Self::Horizontal,
            b'L' => Self::BottomLeft,
            b'J' => Self::BottomRight,
            b'7' => Self::TopRight,
            b'F' => Self::TopLeft,
            _ => Self::Empty,
        }
    }
}

impl Tile {
    pub fn connects(self, from: Direction) -> bool {
        match (from, self) {
            (Direction::Up, Self::Vertical | Self::BottomLeft | Self::BottomRight) => true,
            (Direction::Down, Self::Vertical | Self::TopLeft | Self::TopRight) => true,
            (Direction::Left, Self::Horizontal | Self::TopRight | Self::BottomRight) => true,
            (Direction::Right, Self::Horizontal | Self::TopLeft | Self::BottomLeft) => true,
            _ => false,
        }
    }

    pub fn next(self, from: Direction) -> Option<Direction> {
        match (self, from) {
            (Self::Vertical, Direction::Up) => Some(Direction::Down),
            (Self::Vertical, Direction::Down) => Some(Direction::Up),
            (Self::Horizontal, Direction::Left) => Some(Direction::Right),
            (Self::Horizontal, Direction::Right) => Some(Direction::Left),
            (Self::TopLeft, Direction::Right) => Some(Direction::Down),
            (Self::TopLeft, Direction::Down) => Some(Direction::Right),
            (Self::TopRight, Direction::Left) => Some(Direction::Down),
            (Self::TopRight, Direction::Down) => Some(Direction::Left),
            (Self::BottomLeft, Direction::Right) => Some(Direction::Up),
            (Self::BottomLeft, Direction::Up) => Some(Direction::Right),
            (Self::BottomRight, Direction::Left) => Some(Direction::Up),
            (Self::BottomRight, Direction::Up) => Some(Direction::Left),
            _ => None,
        }
    }

    pub fn rot(self, dir: Direction) -> i64 {
        match (self, dir) {
            (Self::TopLeft, Direction::Right) => 1,
            (Self::TopLeft, Direction::Down) => -1,
            (Self::TopRight, Direction::Left) => -1,
            (Self::TopRight, Direction::Down) => 1,
            (Self::BottomLeft, Direction::Right) => -1,
            (Self::BottomLeft, Direction::Up) => 1,
            (Self::BottomRight, Direction::Left) => 1,
            (Self::BottomRight, Direction::Up) => -1,
            _ => 0,
        }
    }
}
