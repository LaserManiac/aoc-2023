use std::collections::VecDeque;

use crate::day10::INPUT;

pub fn run() -> Option<u64> {
    let mut lines = INPUT.lines().peekable();
    let map_width = lines.peek()?.len();
    let mut map = Vec::with_capacity(20_000);
    let mut start = (0, 0);
    for (y, line) in lines.enumerate() {
        for (x, c) in line.bytes().enumerate() {
            let is_start = c == b'S';
            map.push((Tile::from(c), is_start, is_start));
            if is_start {
                start = (x, y);
            }
        }
    }
    let map_height = map.len() / map_width;
    let idx = |x: usize, y: usize| y * map_width + x;

    let mut queue = VecDeque::with_capacity(100);
    let (x, y) = start;
    let to_visit = [
        (x.saturating_sub(1), y, Direction::Right),
        ((x + 1).min(map_width - 1), y, Direction::Left),
        (x, y.saturating_sub(1), Direction::Down),
        (x, (y + 1).min(map_height - 1), Direction::Up),
    ];
    for (x, y, dir) in to_visit {
        let idx = idx(x, y);
        if map[idx].0.connects(dir) {
            queue.push_front((x, y, 1, dir));
        }
    }

    let mut max_steps = 0;
    while let Some((x, y, i, from)) = queue.pop_front() {
        let tile = &mut map[idx(x, y)];
        tile.1 = true;
        tile.2 = true;
        max_steps = max_steps.max(i);

        let Some(next_dir) = tile.0.next(from) else {
            unreachable!();
        };
        let next_pos = match next_dir {
            Direction::Left => (x.saturating_sub(1), y),
            Direction::Right => ((x + 1).min(map_width - 1), y),
            Direction::Up => (x, y.saturating_sub(1)),
            Direction::Down => (x, (y + 1).min(map_height - 1)),
        };

        if !map[idx(next_pos.0, next_pos.1)].1 {
            queue.push_back((next_pos.0, next_pos.1, i + 1, next_dir.invert()));
        }
    }

    return Some(max_steps);
}

#[derive(Clone, Copy, Debug)]
enum Direction {
    Up,
    Down,
    Left,
    Right,
}

impl Direction {
    pub fn invert(self) -> Direction {
        match self {
            Direction::Left => Direction::Right,
            Direction::Right => Direction::Left,
            Direction::Up => Direction::Down,
            Direction::Down => Direction::Up,
        }
    }
}

#[derive(Clone, Copy, Debug)]
enum Tile {
    Vertical,
    Horizontal,
    TopLeft,
    TopRight,
    BottomLeft,
    BottomRight,
    Empty,
}

impl From<u8> for Tile {
    fn from(value: u8) -> Self {
        match value {
            b'|' => Self::Vertical,
            b'-' => Self::Horizontal,
            b'L' => Self::BottomLeft,
            b'J' => Self::BottomRight,
            b'7' => Self::TopRight,
            b'F' => Self::TopLeft,
            _ => Self::Empty,
        }
    }
}

impl Tile {
    pub fn connects(self, dir: Direction) -> bool {
        match (dir, self) {
            (Direction::Up, Self::Vertical | Self::BottomLeft | Self::BottomRight) => true,
            (Direction::Down, Self::Vertical | Self::TopLeft | Self::TopRight) => true,
            (Direction::Left, Self::Horizontal | Self::TopRight | Self::BottomRight) => true,
            (Direction::Right, Self::Horizontal | Self::TopLeft | Self::BottomLeft) => true,
            _ => false,
        }
    }

    pub fn next(self, dir: Direction) -> Option<Direction> {
        match (self, dir) {
            (Self::Vertical, Direction::Up) => Some(Direction::Down),
            (Self::Vertical, Direction::Down) => Some(Direction::Up),
            (Self::Horizontal, Direction::Left) => Some(Direction::Right),
            (Self::Horizontal, Direction::Right) => Some(Direction::Left),
            (Self::TopLeft, Direction::Right) => Some(Direction::Down),
            (Self::TopLeft, Direction::Down) => Some(Direction::Right),
            (Self::TopRight, Direction::Left) => Some(Direction::Down),
            (Self::TopRight, Direction::Down) => Some(Direction::Left),
            (Self::BottomLeft, Direction::Right) => Some(Direction::Up),
            (Self::BottomLeft, Direction::Up) => Some(Direction::Right),
            (Self::BottomRight, Direction::Left) => Some(Direction::Up),
            (Self::BottomRight, Direction::Up) => Some(Direction::Left),
            _ => None,
        }
    }
}
