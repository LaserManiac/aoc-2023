use crate::day19::{Category, Condition, Names, Operator, Target, Workflow, INPUT};

pub fn run() -> Option<u64> {
    let mut input = INPUT.split("\r\n\r\n");

    let mut names = Names::default();
    let mut workflows = input
        .next()?
        .lines()
        .filter_map(|line| Workflow::parse(line, &mut names))
        .collect::<Vec<_>>();
    workflows.sort_by_cached_key(|wf| wf.id);

    let start = names.get_id("in");
    let mut accepted = Vec::new();
    find_accepted_ranges(start, PartRanges::default(), &workflows, &mut accepted);

    let result = accepted.iter().map(|part| part.permutations()).sum();
    return Some(result);
}

fn find_accepted_ranges(
    id: u64,
    mut part: PartRanges,
    workflows: &[Workflow],
    accepted: &mut Vec<PartRanges>,
) {
    let workflow = &workflows[id as usize];
    for rule in &workflow.rules {
        let constrained = match rule.condition {
            Some(condition) => part.constrained(&condition),
            None => part,
        };

        if constrained.invalid() {
            return;
        }

        if let Target::Accept = rule.target {
            accepted.push(constrained);
        }

        if let Target::Workflow(id) = rule.target {
            find_accepted_ranges(id, constrained, workflows, accepted);
        }

        if let Some(condition) = rule.condition {
            part = part.constrained(&condition.invert());
            if part.invalid() {
                return;
            }
        }
    }
}

#[derive(Clone, Copy, Debug)]
pub struct Range {
    min: u64,
    max: u64,
}

impl Default for Range {
    fn default() -> Self {
        Self { min: 1, max: 4000 }
    }
}

impl Range {
    pub fn valid(&self) -> bool {
        self.min <= self.max
    }

    pub fn permutations(&self) -> u64 {
        self.max - self.min + 1
    }

    pub fn constrained(self, condition: &Condition) -> Self {
        match condition.operator {
            Operator::LessThan => Self {
                min: self.min,
                max: self.max.min(condition.value.saturating_sub(1)),
            },
            Operator::GreaterThan => Self {
                min: self.min.max(condition.value + 1),
                max: self.max,
            },
        }
    }
}

#[derive(Clone, Copy, Default, Debug)]
pub struct PartRanges {
    x: Range,
    m: Range,
    a: Range,
    s: Range,
}

impl PartRanges {
    pub fn invalid(&self) -> bool {
        !self.x.valid() || !self.m.valid() || !self.a.valid() || !self.s.valid()
    }

    pub fn category_range(&mut self, category: Category) -> &mut Range {
        match category {
            Category::X => &mut self.x,
            Category::M => &mut self.m,
            Category::A => &mut self.a,
            Category::S => &mut self.s,
        }
    }

    pub fn constrained(&self, condition: &Condition) -> Self {
        let mut constrained = *self;
        let range = constrained.category_range(condition.category);
        *range = range.constrained(condition);
        return constrained;
    }

    pub fn permutations(&self) -> u64 {
        if self.invalid() {
            unreachable!();
        }

        self.x.permutations()
            * self.m.permutations()
            * self.a.permutations()
            * self.s.permutations()
    }
}
