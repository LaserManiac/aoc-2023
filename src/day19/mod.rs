use std::collections::HashMap;

pub mod a;
pub mod b;

pub const INPUT: &'static str = include_str!("./input.txt");

#[derive(Debug)]
pub struct Workflow {
    id: u64,
    rules: Vec<Rule>,
}

impl Workflow {
    pub fn parse<'a>(source: &'a str, names: &mut Names<'a>) -> Option<Self> {
        let mut source = source.split('{');
        let id = names.get_id(source.next()?);
        let rules = source.next()?;
        let rules = rules[..rules.len() - 1]
            .split(',')
            .filter_map(|rule| Rule::parse(rule, names))
            .collect();
        return Some(Self { id, rules });
    }

    pub fn process(&self, part: &Part) -> Option<Target> {
        self.rules
            .iter()
            .find(|rule| rule.matches(part))
            .map(|rule| rule.target)
    }
}

#[derive(Clone, Copy, Debug)]
pub struct Rule {
    target: Target,
    condition: Option<Condition>,
}

impl Rule {
    pub fn parse<'a>(source: &'a str, names: &mut Names<'a>) -> Option<Self> {
        let mut parts = source.split(':');
        let condition = if source.contains(':') {
            Some(Condition::parse(parts.next()?)?)
        } else {
            None
        };
        let target = match parts.next()? {
            "A" => Target::Accept,
            "R" => Target::Reject,
            name => Target::Workflow(names.get_id(name)),
        };
        return Some(Self { target, condition });
    }

    pub fn matches(&self, part: &Part) -> bool {
        self.condition
            .map(|condition| condition.matches(part))
            .unwrap_or(true)
    }
}

#[derive(Clone, Copy, Debug)]
pub enum Target {
    Workflow(u64),
    Accept,
    Reject,
}

impl Target {
    pub fn is_final(&self) -> bool {
        match self {
            Target::Accept | Target::Reject => true,
            _ => false,
        }
    }
}

#[derive(Clone, Copy, Debug)]
pub struct Condition {
    category: Category,
    operator: Operator,
    value: u64,
}

impl Condition {
    pub fn parse(source: &str) -> Option<Self> {
        let operators = [('>', Operator::GreaterThan), ('<', Operator::LessThan)];
        for (symbol, operator) in operators {
            if source.contains(symbol) {
                let mut source = source.split(symbol);
                let category = match source.next()? {
                    "x" => Category::X,
                    "m" => Category::M,
                    "a" => Category::A,
                    "s" => Category::S,
                    _ => panic!("Invalid category!"),
                };
                let value = source.next()?.parse().ok()?;
                return Some(Self {
                    category,
                    operator,
                    value,
                });
            }
        }

        return None;
    }

    pub fn invert(self) -> Self {
        match self.operator {
            Operator::LessThan => Self {
                category: self.category,
                operator: Operator::GreaterThan,
                value: self.value.saturating_sub(1),
            },
            Operator::GreaterThan => Self {
                category: self.category,
                operator: Operator::LessThan,
                value: self.value + 1,
            },
        }
    }

    pub fn matches(&self, part: &Part) -> bool {
        let category_value = part.category_value(self.category);
        match self.operator {
            Operator::GreaterThan => category_value > self.value,
            Operator::LessThan => category_value < self.value,
        }
    }
}

#[derive(Clone, Copy, Debug)]
pub enum Category {
    X,
    M,
    A,
    S,
}

impl Category {
    pub const ALL: [Category; 4] = [Category::X, Category::M, Category::A, Category::S];
}

#[derive(Clone, Copy, Debug)]
pub enum Operator {
    LessThan,
    GreaterThan,
}

#[derive(Clone, Copy, Debug)]
pub struct Part {
    x: u64,
    m: u64,
    a: u64,
    s: u64,
}

impl Part {
    pub fn parse(source: &str) -> Option<Self> {
        let mut source = source[1..source.len() - 1]
            .split(',')
            .filter_map(|cat| cat.split('=').skip(1).next())
            .map(str::parse::<u64>)
            .filter_map(Result::ok);
        return Some(Self {
            x: source.next()?,
            m: source.next()?,
            a: source.next()?,
            s: source.next()?,
        });
    }

    pub fn category_value(&self, category: Category) -> u64 {
        match category {
            Category::X => self.x,
            Category::M => self.m,
            Category::A => self.a,
            Category::S => self.s,
        }
    }
}

#[derive(Default)]
pub struct Names<'a> {
    counter: u64,
    map: HashMap<&'a str, u64>,
}

impl<'a> Names<'a> {
    pub fn get_id(&mut self, name: &'a str) -> u64 {
        let counter = &mut self.counter;
        return *self.map.entry(name).or_insert_with(|| {
            *counter += 1;
            return *counter - 1;
        });
    }
}
