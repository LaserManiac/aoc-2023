use crate::day19::{Names, Part, Target, Workflow, INPUT};

pub fn run() -> Option<u64> {
    let mut input = INPUT.split("\r\n\r\n");

    let mut names = Names::default();
    let mut workflows = input
        .next()?
        .lines()
        .filter_map(|line| Workflow::parse(line, &mut names))
        .collect::<Vec<_>>();
    workflows.sort_by_cached_key(|wf| wf.id);

    let start_id = names.get_id("in");
    let mut parts = input
        .next()?
        .lines()
        .filter_map(Part::parse)
        .map(|part| (part, start_id))
        .collect::<Vec<_>>();

    let mut accept = Vec::new();
    while let Some((part, mut workflow_id)) = parts.pop() {
        loop {
            let Some(target) = workflows[workflow_id as usize].process(&part) else {
                break;
            };
            match target {
                Target::Workflow(new_workflow_id) => workflow_id = new_workflow_id,
                Target::Accept => accept.push(part),
                Target::Reject => (),
            }
            if target.is_final() {
                break;
            }
        }
    }

    let result = accept
        .into_iter()
        .map(|part| part.x + part.m + part.a + part.s)
        .sum();
    return Some(result);
}
