use std::collections::VecDeque;
use std::str::FromStr;

use crate::day16::{Beam, Direction, Map, Tile, INPUT};

pub fn run() -> Option<u64> {
    let map = Map::from_str(INPUT).ok()?;

    let mut heat_map = vec![0u32; map.width() * map.height()];
    let mut beams = VecDeque::new();

    let initial_beam = Beam::new(0, 0, Direction::Right);
    beams.push_back(initial_beam);

    map.trace(&mut beams, |beam| {
        let idx = beam.y * map.width() + beam.x;
        let visited = heat_map[idx] > 0;

        let stop = match (beam.dir, map.get(beam.x, beam.y)) {
            (Direction::Left | Direction::Right, Tile::SplitterVert) if visited => true,
            (Direction::Up | Direction::Down, Tile::SplitterHor) if visited => true,
            _ => false,
        };

        heat_map[idx] += 1;
        return stop;
    });

    let energized = heat_map.iter().filter(|heat| **heat > 0).count() as u64;
    return Some(energized);
}
