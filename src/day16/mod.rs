use std::collections::VecDeque;
use std::str::FromStr;

pub mod a;
pub mod b;

pub const INPUT: &'static str = include_str!("./input.txt");

pub struct Map {
    width: usize,
    height: usize,
    tiles: Vec<Tile>,
}

impl FromStr for Map {
    type Err = ();

    fn from_str(source: &str) -> Result<Self, Self::Err> {
        let tile_iter = source
            .lines()
            .flat_map(str::as_bytes)
            .copied()
            .map(Tile::from);
        let tiles = Vec::from_iter(tile_iter);
        let height = source.lines().count();
        let width = tiles.len() / height;
        return Ok(Self {
            width,
            height,
            tiles,
        });
    }
}

impl Map {
    pub fn width(&self) -> usize {
        self.width
    }

    pub fn height(&self) -> usize {
        self.height
    }

    pub fn get(&self, x: usize, y: usize) -> Tile {
        self.tiles[y * self.width + x]
    }

    pub fn mov(&self, x: usize, y: usize, dir: Direction) -> Option<(usize, usize)> {
        match dir {
            Direction::Up if y > 0 => Some((x, y - 1)),
            Direction::Left if x > 0 => Some((x - 1, y)),
            Direction::Down if y < self.height - 1 => Some((x, y + 1)),
            Direction::Right if x < self.width - 1 => Some((x + 1, y)),
            _ => None,
        }
    }

    pub fn trace(&self, beams: &mut VecDeque<Beam>, mut stop_fn: impl FnMut(&Beam) -> bool) {
        while let Some(mut beam) = beams.pop_front() {
            loop {
                if stop_fn(&beam) {
                    break;
                }

                let tile = self.get(beam.x, beam.y);
                let (new_dir, split_dir) = tile.interact(beam.dir);

                if let Some(split_dir) = split_dir {
                    if let Some((split_x, split_y)) = self.mov(beam.x, beam.y, split_dir) {
                        let split_beam = Beam::new(split_x, split_y, split_dir);
                        beams.push_back(split_beam);
                    }
                }

                if let Some((x, y)) = self.mov(beam.x, beam.y, new_dir) {
                    beam.x = x;
                    beam.y = y;
                    beam.dir = new_dir;
                    continue;
                };

                break;
            }
        }
    }

    pub fn print(&self, mut override_fn: impl FnMut(usize, usize, Tile) -> Option<char>) {
        let map_str = (0..self.height)
            .flat_map(|y| (0..self.width).map(move |x| Some((x, y))).chain(Some(None)))
            .map(|coord| match coord {
                Some((x, y)) => {
                    let tile = self.get(x, y);
                    return override_fn(x, y, tile).unwrap_or_else(|| tile.into());
                }
                None => '\n',
            })
            .collect::<String>();
        println!("{map_str}\n");
    }
}

#[derive(Clone, Copy, Debug)]
pub enum Direction {
    Up,
    Left,
    Down,
    Right,
}

impl Direction {
    pub fn invert(self) -> Self {
        match self {
            Self::Up => Self::Down,
            Self::Left => Self::Right,
            Self::Down => Self::Up,
            Self::Right => Self::Left,
        }
    }

    pub fn left(self) -> Self {
        match self {
            Self::Up => Self::Left,
            Self::Left => Self::Down,
            Self::Down => Self::Right,
            Self::Right => Self::Up,
        }
    }

    pub fn right(self) -> Self {
        match self {
            Self::Up => Self::Right,
            Self::Left => Self::Up,
            Self::Down => Self::Left,
            Self::Right => Self::Down,
        }
    }
}

#[derive(Clone, Copy, Debug)]
pub struct Beam {
    x: usize,
    y: usize,
    dir: Direction,
}

impl Beam {
    pub fn new(x: usize, y: usize, dir: Direction) -> Self {
        Self { x, y, dir }
    }
}

#[derive(Clone, Copy, Debug)]
pub enum Tile {
    Empty,
    MirrorUp,
    MirrorDown,
    SplitterVert,
    SplitterHor,
}

impl From<u8> for Tile {
    fn from(value: u8) -> Self {
        match value {
            b'/' => Self::MirrorUp,
            b'\\' => Self::MirrorDown,
            b'|' => Self::SplitterVert,
            b'-' => Self::SplitterHor,
            _ => Self::Empty,
        }
    }
}

impl Into<char> for Tile {
    fn into(self) -> char {
        match self {
            Tile::MirrorUp => '/',
            Tile::MirrorDown => '\\',
            Tile::SplitterVert => '|',
            Tile::SplitterHor => '-',
            Tile::Empty => '.',
        }
    }
}

impl Tile {
    pub fn interact(self, dir: Direction) -> (Direction, Option<Direction>) {
        match self {
            Self::MirrorUp => match dir {
                Direction::Up | Direction::Down => (dir.right(), None),
                Direction::Left | Direction::Right => (dir.left(), None),
            },
            Self::MirrorDown => match dir {
                Direction::Up | Direction::Down => (dir.left(), None),
                Direction::Left | Direction::Right => (dir.right(), None),
            },
            Self::SplitterVert => match dir {
                Direction::Up | Direction::Down => (dir, None),
                Direction::Left | Direction::Right => (dir.left(), Some(dir.right())),
            },
            Self::SplitterHor => match dir {
                Direction::Up | Direction::Down => (dir.left(), Some(dir.right())),
                Direction::Left | Direction::Right => (dir, None),
            },
            _ => (dir, None),
        }
    }
}
