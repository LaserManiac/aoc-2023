use std::collections::HashMap;

use crate::day8::INPUT;

pub fn run() -> Option<u64> {
    let mut source = INPUT.split("\r\n\r\n");

    let mut path = Vec::with_capacity(1000);
    path.extend(source.next()?.bytes().map(Direction::from));

    let nodes = source.next()?;
    let nodes = Nodes::parse(nodes)?;

    let mut result = 1;
    for node in nodes.a_nodes() {
        let period = nodes.walk(node, &path)?;
        result = lcm(result, period);
    }

    return Some(result);
}

struct Nodes<'a> {
    index: HashMap<&'a str, usize>,
    data: Vec<(usize, usize, bool, bool)>,
}

impl<'a> Nodes<'a> {
    pub fn parse(source: &'a str) -> Option<Self> {
        let mut this = Self {
            index: HashMap::with_capacity(1000),
            data: Vec::with_capacity(1000),
        };

        for line in source.lines() {
            let mut parts = line.split(" = ");

            let node = parts.next()?;
            let node = this.alloc(node);

            let mut neighbors = parts.next()?.split(", ");

            let left = neighbors.next()?;
            let left = &left[1..];
            let left = this.alloc(left);

            let right = &neighbors.next()?;
            let right = &right[..right.len() - 1];
            let right = this.alloc(right);

            this.set_neighbors(node, left, right);
        }

        return Some(this);
    }

    pub fn a_nodes<'b>(&'b self) -> impl Iterator<Item = usize> + 'b {
        self.data
            .iter()
            .enumerate()
            .filter(|(_, (_, _, a_node, _))| *a_node)
            .map(|(idx, _)| idx)
    }

    pub fn alloc(&mut self, node: &'a str) -> usize {
        *self.index.entry(node).or_insert_with(|| {
            let a_node = node.ends_with('A');
            let z_node = node.ends_with('Z');
            self.data.push((0, 0, a_node, z_node));
            return self.data.len() - 1;
        })
    }

    pub fn set_neighbors(&mut self, node: usize, left: usize, right: usize) {
        let data = &mut self.data[node];
        data.0 = left;
        data.1 = right;
    }

    pub fn walk(&self, node: usize, path: &[Direction]) -> Option<u64> {
        let mut path = path.iter().cycle();
        let mut cur = node;
        let mut counter = 0;
        while !self.data[cur].3 {
            let next = path.next()?;
            cur = match next {
                Direction::Left => self.data[cur].0,
                Direction::Right => self.data[cur].1,
            };
            counter += 1;
        }
        return Some(counter);
    }
}

#[derive(Debug)]
enum Direction {
    Left,
    Right,
}

impl From<u8> for Direction {
    fn from(value: u8) -> Self {
        match value {
            b'L' => Self::Left,
            _ => Self::Right,
        }
    }
}

fn gcd(mut a: u64, mut b: u64) -> u64 {
    while b != 0 {
        let x = b;
        b = a % b;
        a = x;
    }
    return a;
}

fn lcm(a: u64, b: u64) -> u64 {
    return a * b / gcd(a, b);
}
