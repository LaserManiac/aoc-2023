use crate::day8::INPUT;
use std::collections::HashMap;

pub fn run() -> Option<u64> {
    let mut source = INPUT.split("\r\n\r\n");
    let path = source.next()?.bytes().map(|b| {
        if b == b'L' {
            Direction::Left
        } else {
            Direction::Right
        }
    });

    let nodes = source.next()?;
    let nodes = Nodes::parse(nodes)?;

    let start = nodes.get("AAA")?;
    let end = nodes.get("ZZZ")?;
    return nodes.walk(start, end, path);
}

struct Nodes<'a> {
    index: HashMap<&'a str, usize>,
    neighbors: Vec<(usize, usize)>,
}

impl<'a> Nodes<'a> {
    pub fn parse(source: &'a str) -> Option<Self> {
        let mut this = Self {
            index: HashMap::with_capacity(1000),
            neighbors: Vec::with_capacity(1000),
        };

        for line in source.lines() {
            let mut parts = line.split(" = ");

            let node = parts.next()?;
            let node = this.alloc(node);

            let mut neighbors = parts.next()?.split(", ");

            let left = neighbors.next()?;
            let left = &left[1..];
            let left = this.alloc(left);

            let right = &neighbors.next()?;
            let right = &right[..right.len() - 1];
            let right = this.alloc(right);

            this.set_neighbors(node, left, right);
        }

        return Some(this);
    }

    pub fn get(&self, node: &'a str) -> Option<usize> {
        self.index.get(node).copied()
    }

    pub fn alloc(&mut self, node: &'a str) -> usize {
        *self.index.entry(node).or_insert_with(|| {
            self.neighbors.push((0, 0));
            return self.neighbors.len() - 1;
        })
    }

    pub fn set_neighbors(&mut self, node: usize, left: usize, right: usize) {
        self.neighbors[node] = (left, right);
    }

    pub fn walk(
        &self,
        start: usize,
        end: usize,
        path: impl Iterator<Item = Direction> + Clone,
    ) -> Option<u64> {
        let mut path = path.cycle();
        let mut cur = start;
        let mut counter = 0;
        while cur != end {
            match path.next()? {
                Direction::Left => cur = self.neighbors[cur].0,
                Direction::Right => cur = self.neighbors[cur].1,
            }
            counter += 1;
        }
        return Some(counter);
    }
}

#[derive(Debug)]
enum Direction {
    Left,
    Right,
}
