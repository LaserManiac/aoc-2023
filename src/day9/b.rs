use crate::day9::INPUT;

pub fn run() -> Option<u64> {
    let mut cache = Vec::with_capacity(1000);
    let result = INPUT
        .lines()
        .filter_map(|line| analyze_sequence(line, &mut cache))
        .sum::<i64>();
    return Some(result as u64);
}

fn analyze_sequence(source: &str, cache: &mut Vec<i64>) -> Option<i64> {
    cache.clear();
    cache.extend(
        source
            .split(' ')
            .map(str::parse::<i64>)
            .filter_map(Result::ok),
    );

    let mut sum = 0;
    let mut iter = 0;
    loop {
        let first = cache[0];
        sum += first;
        iter += 1;

        for i in 0..cache.len() - iter {
            cache[i] = cache[i] - cache[i + 1];
        }

        if cache[..cache.len() - iter].iter().all(|value| *value == 0) {
            break;
        }
    }

    return Some(sum);
}
