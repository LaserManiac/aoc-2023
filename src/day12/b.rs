use std::collections::HashMap;

use crate::day12::INPUT;

pub fn run() -> u64 {
    let mut conditions = Vec::new();
    let mut groups = Vec::new();
    let mut cache = HashMap::new();
    INPUT
        .lines()
        .filter_map(|line| {
            let mut parts = line.split(' ');

            conditions.clear();
            let conditions_iter = parts.next()?.bytes();
            conditions.extend(
                conditions_iter
                    .clone()
                    .chain(Some(UNDETERMINED))
                    .chain(conditions_iter.clone())
                    .chain(Some(UNDETERMINED))
                    .chain(conditions_iter.clone())
                    .chain(Some(UNDETERMINED))
                    .chain(conditions_iter.clone())
                    .chain(Some(UNDETERMINED))
                    .chain(conditions_iter),
            );

            groups.clear();
            let groups_iter = parts
                .next()?
                .split(',')
                .map(str::parse::<u8>)
                .filter_map(Result::ok);
            groups.extend(
                groups_iter
                    .clone()
                    .chain(groups_iter.clone())
                    .chain(groups_iter.clone())
                    .chain(groups_iter.clone())
                    .chain(groups_iter),
            );

            cache.clear();
            let result = collapse(&mut conditions, 0, &groups, 0, &mut cache);
            return Some(result);
        })
        .sum::<u64>()
}

fn collapse(
    conditions: &mut [u8],
    mut idx: usize,
    groups: &[u8],
    mut group_idx: usize,
    cache: &mut HashMap<(u8, usize, usize), u64>,
) -> u64 {
    let key = (conditions[idx], idx, group_idx);
    if let Some(result) = cache.get(&key) {
        return *result;
    }

    let mut expect_break = false;
    let result = 'outer: loop {
        match conditions[idx] {
            spring if spring == DAMAGED => {
                if group_idx == groups.len() || expect_break {
                    break 0;
                }

                let group_count = groups[group_idx] as usize;
                if idx + group_count > conditions.len() {
                    break 0;
                }

                for i in 1..group_count {
                    if conditions[idx + i] == OPERATIONAL {
                        break 'outer 0;
                    }
                }

                idx += group_count;
                group_idx += 1;
                expect_break = true;
            }
            spring if spring == OPERATIONAL => {
                idx += 1;
                expect_break = false;
            }
            _ => {
                let mut result = 0;

                if !expect_break {
                    conditions[idx] = DAMAGED;
                    result += collapse(conditions, idx, groups, group_idx, cache);
                }

                conditions[idx] = OPERATIONAL;
                result += collapse(conditions, idx, groups, group_idx, cache);

                conditions[idx] = UNDETERMINED;
                break result;
            }
        }

        if idx >= conditions.len() {
            return match group_idx == groups.len() {
                true => 1,
                false => 0,
            };
        }
    };

    cache.insert(key, result);
    return result;
}

const OPERATIONAL: u8 = b'.';
const DAMAGED: u8 = b'#';
const UNDETERMINED: u8 = b'?';
