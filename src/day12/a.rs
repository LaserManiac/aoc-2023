use crate::day12::INPUT;

pub fn run() -> u64 {
    let mut conditions = Vec::<u8>::new();
    let mut groups = Vec::<u8>::new();
    INPUT
        .lines()
        .filter_map(|line| {
            let mut parts = line.split(' ');

            conditions.clear();
            conditions.extend(parts.next()?.bytes());

            groups.clear();
            groups.extend(
                parts
                    .next()?
                    .split(',')
                    .map(str::parse::<u8>)
                    .filter_map(Result::ok),
            );

            return Some(collapse(&mut conditions, 0, &groups, 0, None));
        })
        .sum::<u64>()
}

fn collapse(
    conditions: &mut [u8],
    idx: usize,
    groups: &[u8],
    group_idx: usize,
    group_count: Option<u8>,
) -> u64 {
    if idx >= conditions.len() {
        let last_group = group_idx == groups.len() - 1;
        let end_of_groups = group_idx == groups.len();
        let x = match group_count {
            Some(group_count) if group_count == groups[group_idx] && last_group => 1,
            None if end_of_groups => 1,
            _ => 0,
        };
        return x;
    }

    let spring = conditions[idx];

    if spring == DAMAGED {
        if group_idx >= groups.len() {
            return 0;
        }

        let new_group_count = match group_count {
            Some(group_count) => {
                if group_count >= groups[group_idx] {
                    return 0;
                }
                Some(group_count + 1)
            }
            None => Some(1),
        };

        return collapse(conditions, idx + 1, groups, group_idx, new_group_count);
    }

    if spring == OPERATIONAL {
        let new_group_idx = match group_count {
            Some(group_count) => {
                if group_count != groups[group_idx] {
                    return 0;
                }
                group_idx + 1
            }
            None => group_idx,
        };

        return collapse(conditions, idx + 1, groups, new_group_idx, None);
    }

    let mut sum = 0;
    for spring in [OPERATIONAL, DAMAGED] {
        conditions[idx] = spring;
        sum += collapse(conditions, idx, groups, group_idx, group_count);
    }
    conditions[idx] = UNDETERMINED;
    return sum;
}

const OPERATIONAL: u8 = b'.';
const DAMAGED: u8 = b'#';
const UNDETERMINED: u8 = b'?';
