use std::collections::{HashMap, HashSet};

use crate::day4::INPUT;

pub fn run() -> u64 {
    let mut cache = HashSet::with_capacity(100);
    let cards = INPUT
        .lines()
        .filter_map(|line| parse_line(line, &mut cache))
        .collect::<HashMap<u32, u32>>();
    let mut cache = HashMap::new();
    return (1..=cards.len() as u32)
        .map(|id| card_count(id, &cards, &mut cache))
        .sum::<u32>() as u64;
}

fn card_count(id: u32, cards: &HashMap<u32, u32>, cache: &mut HashMap<u32, u32>) -> u32 {
    if let Some(cache) = cache.get(&id) {
        return *cache;
    }

    let matches = cards[&id];
    let extra = (id + 1..=id + matches)
        .map(|id| card_count(id, cards, cache))
        .sum::<u32>();
    let total = extra + 1;
    cache.insert(id, total);
    return total;
}

fn parse_line(line: &str, cache: &mut HashSet<u32>) -> Option<(u32, u32)> {
    let mut card = line.split(": ");
    let id = card.next()?.split(' ').rev().next()?.parse::<u32>().ok()?;
    let mut numbers = card.next()?.split(" | ");

    let winning = cache;
    winning.clear();
    winning.extend(
        numbers
            .next()?
            .split(" ")
            .map(str::trim)
            .map(str::parse::<u32>)
            .filter_map(Result::ok),
    );

    let matches = numbers
        .next()?
        .split(" ")
        .map(str::trim)
        .map(str::parse::<u32>)
        .filter_map(Result::ok)
        .filter(|number| winning.contains(number))
        .count();
    return Some((id, matches as u32));
}
