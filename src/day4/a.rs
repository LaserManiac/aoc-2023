use crate::day4::INPUT;
use std::collections::HashSet;

pub fn run() -> u64 {
    let mut cache = HashSet::with_capacity(100);
    return INPUT
        .lines()
        .filter_map(|line| parse_line(line, &mut cache))
        .sum::<u32>() as u64;
}

fn parse_line(line: &str, cache: &mut HashSet<u32>) -> Option<u32> {
    let card = line.split(": ").skip(1).next()?;
    let mut numbers = card.split(" | ");

    let winning = cache;
    winning.clear();
    winning.extend(
        numbers
            .next()?
            .split(" ")
            .map(str::trim)
            .map(str::parse::<u32>)
            .filter_map(Result::ok),
    );

    let matches = numbers
        .next()?
        .split(" ")
        .map(str::trim)
        .map(str::parse::<u32>)
        .filter_map(Result::ok)
        .filter(|number| winning.contains(number))
        .count();
    return (matches > 0).then(|| 2u32.pow((matches - 1) as u32));
}
