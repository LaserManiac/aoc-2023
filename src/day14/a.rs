use crate::day14::INPUT;
use crate::day14::{Direction, Map};

pub fn run() -> u64 {
    let (mut map, mut rounds) = Map::new(INPUT);
    map.tilt(Direction::North, &mut rounds);
    return map.load(Direction::North, &rounds);
}
