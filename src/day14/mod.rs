pub mod a;
pub mod b;

pub const INPUT: &'static str = include_str!("./input.txt");

type Unit = u8;

pub struct Map {
    width: usize,
    height: usize,
    rocks: Vec<Option<Rock>>,
    cache: Vec<Unit>,
}

impl Map {
    pub fn new(source: &str) -> (Self, Rounds) {
        let mut height = 0;
        let mut rocks = Vec::new();
        let mut round = Vec::new();
        for (y, line) in source.lines().enumerate() {
            height += 1;
            for (x, c) in line.bytes().enumerate() {
                let rock = match c {
                    b'#' => Some(Rock::Cube),
                    b'O' => Some(Rock::Round),
                    _ => None,
                };

                rocks.push(rock);

                if let Some(Rock::Round) = rock {
                    round.push(x as Unit);
                    round.push(y as Unit);
                }
            }
        }
        let width = rocks.len() / height;
        let cache = vec![0; rocks.len()];

        let this = Self {
            width,
            height,
            rocks,
            cache,
        };
        let rounds = Rounds { rocks: round };
        return (this, rounds);
    }

    pub fn get(&self, x: Unit, y: Unit) -> Option<Rock> {
        self.rocks[y as usize * self.width + x as usize]
    }

    pub fn set(&mut self, x: Unit, y: Unit, rock: Option<Rock>) {
        self.rocks[y as usize * self.width + x as usize] = rock;
    }

    pub fn load_at(&self, x: Unit, y: Unit, dir: Direction) -> Unit {
        match dir {
            Direction::North => self.height as Unit - y,
            Direction::West => x + 1,
            Direction::South => y + 1,
            Direction::East => self.width as Unit - x,
        }
    }

    pub fn load(&self, dir: Direction, rounds: &Rounds) -> u64 {
        rounds
            .iter()
            .map(|(x, y)| self.load_at(*x, *y, dir) as u64)
            .sum()
    }

    pub fn tilt(&mut self, dir: Direction, rounds: &mut Rounds) {
        let (sweep_length, sweep_size) = match dir {
            Direction::North | Direction::South => (self.height, self.width),
            Direction::West | Direction::East => (self.width, self.height),
        };

        for sweep_iter in 0..sweep_size as Unit {
            let mut counter = 0;
            for sweep_trav in 0..sweep_length as Unit {
                let (x, y) = match dir {
                    Direction::North => (sweep_iter, sweep_trav),
                    Direction::West => (sweep_trav, sweep_iter),
                    Direction::South => (sweep_iter, self.height as Unit - 1 - sweep_trav),
                    Direction::East => (self.width as Unit - 1 - sweep_trav, sweep_iter),
                };
                let idx = y as usize * self.width + x as usize;
                self.cache[idx] = counter;
                match self.get(x, y) {
                    Some(Rock::Cube) => counter = 0,
                    Some(Rock::Round) => (),
                    None => counter += 1,
                }
            }
        }

        for (x, y) in rounds.iter() {
            self.set(*x, *y, None);
        }

        for (x, y) in rounds.iter_mut() {
            let idx = *y as usize * self.width + *x as usize;
            let move_amount = self.cache[idx];
            match dir {
                Direction::North => *y -= move_amount,
                Direction::West => *x -= move_amount,
                Direction::South => *y += move_amount,
                Direction::East => *x += move_amount,
            }
            self.set(*x, *y, Some(Rock::Round));
        }
    }
}

#[derive(Clone, Eq)]
pub struct Rounds {
    rocks: Vec<Unit>,
}

impl Rounds {
    pub fn iter<'a>(&'a self) -> impl Iterator<Item = (&'a Unit, &'a Unit)> + 'a {
        self.rocks.chunks(2).map(|coord| (&coord[0], &coord[1]))
    }

    pub fn iter_mut<'a>(&'a mut self) -> impl Iterator<Item = (&'a mut Unit, &'a mut Unit)> + 'a {
        self.rocks
            .chunks_mut(2)
            .map(|coord| coord.split_at_mut(1))
            .map(|coord| (&mut coord.0[0], &mut coord.1[0]))
    }
}

impl PartialEq for Rounds {
    fn eq(&self, other: &Self) -> bool {
        self.rocks.as_slice() == other.rocks.as_slice()
    }
}

#[derive(Clone, Copy)]
pub enum Rock {
    Round,
    Cube,
}

#[derive(Clone, Copy, PartialEq, Eq, Hash)]
pub enum Direction {
    North,
    West,
    South,
    East,
}

impl Direction {
    pub fn next(self) -> Self {
        match self {
            Self::North => Self::West,
            Self::West => Self::South,
            Self::South => Self::East,
            Self::East => Self::North,
        }
    }
}
