use crate::day14::INPUT;
use crate::day14::{Direction, Map};

pub fn run() -> u64 {
    let (mut map, mut rounds) = Map::new(INPUT);
    let mut load_history = Vec::new();
    let mut dir = Direction::North;
    return 'main: loop {
        for _ in 0..4 {
            map.tilt(dir, &mut rounds);
            dir = dir.next();
        }

        let idx = load_history.len();
        let load = map.load(dir, &rounds);
        load_history.push(load);

        'outer: for prev_last in (0..load_history.len() - 1).rev() {
            let period = idx - prev_last;
            if period == 1 || load_history.len() < period * 2 {
                continue 'outer;
            }

            for j in 0..period {
                let previous_load = load_history[idx - period - j];
                let repeat_load = load_history[idx - j];
                if previous_load != repeat_load {
                    continue 'outer;
                }
            }

            let initial = idx - period * 2;
            let rem = (1_000_000_000 - initial - 1) % period;
            let target = initial + rem;

            let target_load = load_history[target];
            break 'main target_load;
        }
    };
}
