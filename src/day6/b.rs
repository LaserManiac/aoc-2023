use crate::day6::INPUT;

pub fn run() -> u64 {
    return exec().unwrap();
}

fn exec() -> Option<u64> {
    let mut lines = INPUT.lines().filter_map(|line| {
        line.split(": ")
            .skip(1)
            .map(str::trim)
            .next()?
            .chars()
            .filter_map(|c| c.to_digit(10))
            .map(|x| x as u64)
            .reduce(|a, b| a * 10 + b)
    });
    let time = lines.next()?;
    let distance = lines.next()?;
    let result = get_possible(time, distance);
    return Some(result);
}

fn get_possible(time: u64, best: u64) -> u64 {
    let d = time * time - 4 * best;
    let sqrt = f64::sqrt(d as f64);
    let t1 = ((time as f64 - sqrt) / 2.0).floor() as u64 + 1;
    let t2 = ((time as f64 + sqrt) / 2.0).ceil() as u64 - 1;
    return t2.min(time) - t1 + 1;
}
