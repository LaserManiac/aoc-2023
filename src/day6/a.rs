use crate::day6::INPUT;

pub fn run() -> u64 {
    return exec().unwrap();
}

fn exec() -> Option<u64> {
    let mut lines = INPUT.lines().filter_map(|line| {
        line.split(": ")
            .skip(1)
            .map(str::trim)
            .next()?
            .split(' ')
            .map(str::trim)
            .map(str::parse::<u64>)
            .filter_map(Result::ok)
            .into()
    });
    let time = lines.next()?;
    let distance = lines.next()?;
    let result = time
        .zip(distance)
        .map(|(time, best)| get_possible(time, best))
        .product::<u64>();
    return Some(result);
}

fn get_possible(time: u64, best: u64) -> u64 {
    let d = time * time - 4 * best;
    let sqrt = f32::sqrt(d as f32);
    let t1 = ((time as f32 - sqrt) / 2.0).floor() as u64 + 1;
    let t2 = ((time as f32 + sqrt) / 2.0).ceil() as u64 - 1;
    return t2.min(time) - t1 + 1;
}
