use std::collections::{HashMap, VecDeque};
use std::marker::PhantomData;

pub mod a;
pub mod b;

pub const INPUT: &'static str = include_str!("./input.txt");

#[derive(Debug)]
pub struct Machine<'a, S> {
    modules: Modules<'a>,
    inputs: Vec<ModuleInput>,
    outputs: Vec<ModuleOutput>,
    _marker: PhantomData<S>,
}

impl<'a> Machine<'a, NotReady> {
    pub fn new() -> Self {
        Self {
            modules: Modules::default(),
            outputs: Vec::default(),
            inputs: Vec::default(),
            _marker: Default::default(),
        }
    }

    pub fn add_module(
        &mut self,
        kind: ModuleKind,
        name: &'a str,
        outputs: impl Iterator<Item = &'a str>,
    ) {
        let module_id = self.modules.register(name);
        let output_start_id = self.outputs.len();
        for output_name in outputs {
            let output_module_id = self.modules.register(output_name);
            let output_id = self.outputs.len();
            self.outputs.push(ModuleOutput {
                module_id,
                input_id: usize::MAX,
            });
            self.inputs.push(ModuleInput {
                module_id: output_module_id,
                output_id,
            });
        }
        let output_end_id = self.outputs.len();
        *self.modules.get_mut(module_id) = Module {
            kind,
            output_start_id,
            output_end_id,
            input_start_id: usize::MAX,
            input_end_id: usize::MIN,
        };
    }

    pub fn map_io(mut self) -> Machine<'a, Ready> {
        self.inputs.sort_by_cached_key(|input| input.module_id);

        for (input_id, input) in self.inputs.iter().enumerate() {
            let output = &mut self.outputs[input.output_id];
            output.input_id = input_id;
        }

        for (input_id, input) in self.inputs.iter().enumerate() {
            let module = self.modules.get_mut(input.module_id);
            module.input_start_id = module.input_start_id.min(input_id);
            module.input_end_id = module.input_end_id.max(input_id + 1);
        }

        for module in self.modules.iter_mut() {
            module.input_start_id = module.input_start_id.min(module.input_end_id);
        }

        return Machine {
            modules: self.modules,
            outputs: self.outputs,
            inputs: self.inputs,
            _marker: Default::default(),
        };
    }
}

impl<'a> Machine<'a, Ready> {
    pub fn parse(source: &'a str) -> Option<Self> {
        let mut machine = Machine::new();

        for line in source.lines() {
            let mut parts = line.split(" -> ");
            let name = parts.next()?;
            let (name, kind) = if name.starts_with('%') {
                (&name[1..], ModuleKind::FlipFlop)
            } else if name.starts_with('&') {
                (&name[1..], ModuleKind::Conjunction)
            } else {
                (name, ModuleKind::Broadcast)
            };
            let outputs = parts.next()?.split(", ");
            machine.add_module(kind, name, outputs);
        }

        return Some(machine.map_io());
    }

    pub fn module_id(&self, name: &'a str) -> usize {
        self.modules.id(name)
    }

    pub fn init_state(&self) -> MachineState {
        MachineState {
            modules: vec![ModuleState::Off; self.modules.len()],
            inputs: vec![Pulse::Low; self.inputs.len()],
        }
    }

    pub fn run(
        &self,
        pulse: Pulse,
        target_id: usize,
        state: &mut MachineState,
        queue: &mut VecDeque<(usize, Pulse)>,
        mut f: impl FnMut(&ModuleInput, Pulse),
    ) {
        let module = self.modules.get(target_id);
        for i in module.output_start_id..module.output_end_id {
            let output = &self.outputs[i];
            queue.push_back((output.input_id, pulse));
        }

        while let Some((input_id, pulse)) = queue.pop_front() {
            let input = &self.inputs[input_id];
            state.inputs[input_id] = pulse;

            f(input, pulse);

            let module = self.modules.get(input.module_id);
            let pulse = match &module.kind {
                ModuleKind::Broadcast => Some(pulse),
                ModuleKind::FlipFlop => match pulse {
                    Pulse::High => None,
                    Pulse::Low => {
                        let state = &mut state.modules[input.module_id];
                        *state = state.inv();
                        (*state == ModuleState::On)
                            .then_some(Pulse::High)
                            .or(Some(Pulse::Low))
                    }
                },
                ModuleKind::Conjunction => {
                    let input_states = &state.inputs[module.input_start_id..module.input_end_id];
                    let all_high = input_states.iter().all(|state| *state == Pulse::High);
                    all_high.then_some(Pulse::Low).or(Some(Pulse::High))
                }
            };

            if let Some(pulse) = pulse {
                for i in module.output_start_id..module.output_end_id {
                    let output = &self.outputs[i];
                    queue.push_back((output.input_id, pulse));
                }
            }
        }
    }

    // pub fn pulse_req(&self, module_id: usize, pulse: Pulse, state: MachineState) -> usize {
    //     let module = &self.modules[module_id];
    //     match module.kind {
    //         ModuleKind::Broadcast => {
    //             for input_id in module.input_start_id..module.input_end_id {
    //                 state.inputs[input_id] = pulse;
    //             }
    //             return self.inputs[module.input_start_id..module.input_end_id]
    //                 .iter()
    //                 .map(|input| {
    //                     self.pulse_req(self.outputs[input.output_id].module_id, pulse, state)
    //                 })
    //                 .min()
    //                 .unwrap();
    //         }
    //         ModuleKind::FlipFlop => connected
    //             .map(|module_id| self.pulse_req(module_id) * 2)
    //             .min()
    //             .unwrap_or(1),
    //         ModuleKind::Conjunction => connected
    //             .map(|module_id| self.pulse_req(module_id))
    //             .fold(1, |acc, period| lcm(acc, period)),
    //     }
    // }

    // fn collapse_state(
    //     &self,
    //     target_id: usize,
    //     target_pulse: Pulse,
    //     source_id: usize,
    //     source_pulse: Pulse,
    //     state: MachineState,
    //     universe: usize,
    //     counts: &mut HashMap<(usize, usize, usize), usize>,
    // ) {
    //     let module = &self.modules[target_id];
    //     match module.kind {
    //         ModuleKind::Noop => {
    //             for input_id in module.input_start_id..module.input_end_id {
    //                 let input = &self.inputs[input_id];
    //                 let output = &self.outputs[input.output_id];
    //
    //                 let mut state = state.clone();
    //                 state.inputs[input_id] = target_pulse;
    //
    //                 self.collapse_state(
    //                     output.module_id,
    //                     target_pulse,
    //                     source_id,
    //                     source_pulse,
    //                     state,
    //                     universe + 1,
    //                     counts,
    //                 );
    //             }
    //         }
    //         ModuleKind::Broadcast => (),
    //         ModuleKind::FlipFlop => {
    //             let ff_state = state.modules[target_id];
    //             state.modules[target_id] = ff_state.inv();
    //             match (target_pulse, ff_state) {
    //                 (Pulse::Low, ModuleState::Off) | (Pulse::High, ModuleState::On) => {
    //                     for input_id in module.input_start_id..module.input_end_id {
    //                         let input = &self.inputs[input_id];
    //                         let output = &self.outputs[input.output_id];
    //
    //                         let mut state = state.clone();
    //                         state.inputs[input_id] = Pulse::Low;
    //
    //                         self.collapse_state(
    //                             output.module_id,
    //                             Pulse::Low,
    //                             source_id,
    //                             source_pulse,
    //                             state,
    //                             universe + 1,
    //                             counts,
    //                         );
    //                     }
    //                 }
    //                 (Pulse::Low, ModuleState::On) | (Pulse::High, ModuleState::Off) => {
    //                     self.collapse_state(
    //                         target_id,
    //                         Pulse::Low,
    //                         source_id,
    //                         source_pulse,
    //                         state,
    //                         universe + 1,
    //                         counts,
    //                     );
    //                 }
    //             }
    //         }
    //         ModuleKind::Conjunction => (),
    //     }
    // }
}

#[derive(Clone, Debug)]
pub struct MachineState {
    modules: Vec<ModuleState>,
    inputs: Vec<Pulse>,
}

#[derive(Debug)]
pub struct NotReady;
#[derive(Debug)]
pub struct Ready;

#[derive(Debug)]
pub struct Module {
    kind: ModuleKind,
    input_start_id: usize,
    input_end_id: usize,
    output_start_id: usize,
    output_end_id: usize,
}

impl Module {
    pub fn broadcast() -> Self {
        Self {
            kind: ModuleKind::Broadcast,
            output_start_id: 0,
            output_end_id: 0,
            input_start_id: usize::MAX,
            input_end_id: usize::MIN,
        }
    }
}

#[derive(Debug)]
pub enum ModuleKind {
    Broadcast,
    FlipFlop,
    Conjunction,
}

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub enum ModuleState {
    On,
    Off,
}

impl ModuleState {
    pub fn inv(self) -> Self {
        match self {
            Self::On => Self::Off,
            Self::Off => Self::On,
        }
    }
}

#[derive(Debug)]
pub struct ModuleInput {
    module_id: usize,
    output_id: usize,
}

#[derive(Debug)]
pub struct ModuleOutput {
    module_id: usize,
    input_id: usize,
}

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub enum Pulse {
    Low,
    High,
}

impl Pulse {
    pub fn inv(self) -> Self {
        match self {
            Self::Low => Self::High,
            Self::High => Self::Low,
        }
    }
}

#[derive(Default, Debug)]
pub struct Modules<'a> {
    modules: Vec<Module>,
    map: HashMap<&'a str, usize>,
    names: Vec<&'a str>,
}

impl<'a> Modules<'a> {
    pub fn register(&mut self, name: &'a str) -> usize {
        let modules = &mut self.modules;
        let names = &mut self.names;
        return *self.map.entry(name).or_insert_with(|| {
            modules.push(Module::broadcast());
            names.push(name);
            return modules.len() - 1;
        });
    }

    pub fn id(&self, name: &'a str) -> usize {
        self.map[name]
    }

    pub fn name(&self, id: usize) -> &'a str {
        self.names[id]
    }

    pub fn get(&self, id: usize) -> &Module {
        &self.modules[id]
    }

    pub fn get_mut(&mut self, id: usize) -> &mut Module {
        &mut self.modules[id]
    }
}

impl<'a> std::ops::Deref for Modules<'a> {
    type Target = [Module];

    fn deref(&self) -> &Self::Target {
        &self.modules
    }
}

impl<'a> std::ops::DerefMut for Modules<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.modules
    }
}

fn gcd(mut a: usize, mut b: usize) -> usize {
    while b != 0 {
        let x = b;
        b = a % b;
        a = x;
    }
    return a;
}

fn lcm(a: usize, b: usize) -> usize {
    return a * b / gcd(a, b);
}
