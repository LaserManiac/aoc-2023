use crate::day20::{Machine, Pulse, INPUT};

pub fn run() -> Option<u64> {
    let machine = Machine::parse(INPUT)?;

    let broadcaster_id = machine.module_id("broadcaster");
    let mut low_count = 0;
    let mut high_count = 0;
    let mut state = machine.init_state();
    let mut queue = Default::default();
    for _ in 0..1000 {
        low_count += 1;
        machine.run(
            Pulse::Low,
            broadcaster_id,
            &mut state,
            &mut queue,
            |_, pulse| match pulse {
                Pulse::Low => low_count += 1,
                Pulse::High => high_count += 1,
            },
        );
    }
    return Some(low_count * high_count);
}
