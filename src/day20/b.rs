use crate::day20::{Machine, Pulse, INPUT};

pub fn run() -> Option<u64> {
    let machine = Machine::parse(INPUT)?;

    // let mut state = machine.init_state();
    // let rx_id = machine.module_id("rx");
    // let result = machine.pulse_req(rx_id, Pulse::Low, state);

    // let broadcaster_id = machine.module_id("broadcaster");
    // let mut state = machine.init_state();
    // let mut queue = Default::default();
    // let mut result = 0;
    // let mut stop = false;
    // while !stop {
    //     result += 1;
    //     machine.run(
    //         Pulse::Low,
    //         broadcaster_id,
    //         &mut state,
    //         &mut queue,
    //         |input, pulse| {
    //             if input.module_id == rx_id && pulse == Pulse::Low {
    //                 stop = true;
    //             }
    //         },
    //     );
    // }

    return None;
}
